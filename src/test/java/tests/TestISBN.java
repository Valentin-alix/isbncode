package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import epsi.tp.model.ISBN;

//TimeOut to achieve 5000 tests per minuts

@Timeout(value = 720, unit = TimeUnit.MILLISECONDS)
public class TestISBN {

	// public static ISBN isbn;

	@BeforeClass
	public static void setUp() {
		// isbn = new ISBN();
	}

	@ParameterizedTest
	@CsvSource({ "0-2593-1561-3, 0259315613" })
	public void TestNormalize(String input, String expected) {
		assertEquals(ISBN.Normalize(input), expected);
	}

	@ParameterizedTest
	@CsvSource({ "0-2593-1561-3, 10" })
	public void TestgetSize(String input, int expected) {
		assertEquals(ISBN.getSize(input), expected);
	}

	@ParameterizedTest
	@CsvSource({ "0-2593-1561-3, true", "pjdsjcjdcj, false", "0-2593-1561-3p, false" })
	public void TestcharacterValid(String input, boolean expected) {
		assertEquals(ISBN.characterValid(input), expected);
	}

	@ParameterizedTest
	@CsvSource({ "0-2593-1561-3, 4", "0-2593-1561-35-56, 5" })
	public void TestgoodNumberSegment(String input, int expected) {
		assertEquals(ISBN.getNumberSegment(input), expected);
	}

	@ParameterizedTest
	@CsvSource({ "0-2593-1561-3, 4", "0-2593-1561-35-56, 5" })
	public void TestgoodFormat(String input, int expected) {

	}

	@ParameterizedTest
	@CsvFileSource(resources = "/isbn10.csv", delimiter = ';')
	public void TestisValid10(String input, Boolean expected) {
		assertEquals(ISBN.isValidISBN10(input), expected);
	}

	@ParameterizedTest
	// @Disabled
	@CsvFileSource(resources = "../isbn13.csv", delimiter = ';')
	public void TestisValid13(String input, Boolean expected) {
		assertEquals(ISBN.isValidISBN13(input), expected);
	}
}
