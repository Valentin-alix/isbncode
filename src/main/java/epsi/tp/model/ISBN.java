package epsi.tp.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Getter;

@Getter
public class ISBN {

	private String isbnValue;
	private int isbnSize;

	public ISBN() {

	};

	public ISBN(String inputISBN) throws Exception {
		if (isValid(inputISBN)) {
			this.isbnValue = inputISBN;
		} else {
			throw new Exception("Erreur code ISBN non valide");
		}

	};

	public void setISBN(String inputISBN) throws Exception {
		if (isValid(inputISBN)) {
			this.isbnValue = inputISBN;
		} else {
			throw new Exception("Erreur code ISBN non valide");
		}

	};

	public static String Normalize(String inputISBN) {
		return inputISBN.replaceAll("[-]", "");
	};

	public static int getSize(String inputISBN) {

		return ISBN.Normalize(inputISBN).length();
	};

	public static boolean characterValid(String inputISBN) {
		Pattern regex = Pattern.compile("[^0-9-X]");
		Matcher regexMatcher = regex.matcher(inputISBN);
		return !regexMatcher.find();
	}

	public static int getNumberSegment(String inputISBN) {
		int numberHyphen = 0;
		for (int i = 0; i < inputISBN.length(); i++) {
			if (inputISBN.charAt(i) == '-') {
				numberHyphen++;
			}
		}
		return numberHyphen + 1;
	}

	public static boolean isValidISBN10(String isbnToValidate) {

		isbnToValidate = Normalize(isbnToValidate);
		int somme = 0;
		for (int i = 0; i < isbnToValidate.length() - 1; i++) {
			somme += (i + 1) * Integer.parseInt(isbnToValidate.substring(i, i + 1));
		}

		String checksum = isbnToValidate.substring(isbnToValidate.length() - 1, isbnToValidate.length());
		if (checksum.equals("X")) {
			checksum = "10";
		}
		return somme % 11 == Integer.parseInt(checksum);

	};

	public static boolean isValidISBN13(String isbnToValidate) {

		isbnToValidate = Normalize(isbnToValidate);
		int somme = 0;
		int multiplicator = 0;
		for (int i = 0; i < isbnToValidate.length() - 1; i++) {
			if ((i + 1) % 2 == 0) {
				multiplicator = 3;
			} else {
				multiplicator = 1;
			}
			somme += Integer.parseInt(isbnToValidate.substring(i, i + 1)) * multiplicator;
		}

		String checksum = isbnToValidate.substring(isbnToValidate.length() - 1, isbnToValidate.length());
		switch (checksum) {
		case "X":
			checksum = "10";
			break;
		case "0":
			return somme % 10 == Integer.parseInt(checksum);
		}
		return (10 - somme % 10 == Integer.parseInt(checksum));
	};

	public boolean isValid(String InputIsbn) {
		if (characterValid(InputIsbn)) {
			if (getSize(InputIsbn) == 10 && getNumberSegment(InputIsbn) == 4) {
				return isValidISBN10(InputIsbn);
			} else if (getSize(InputIsbn) == 13 && getNumberSegment(InputIsbn) == 5) {
				return isValidISBN13(InputIsbn);
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
